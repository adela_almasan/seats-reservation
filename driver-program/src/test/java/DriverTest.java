import static org.hamcrest.Matchers.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import seatsreservation.seatingchart.DefaultSeatingChart;
import seatsreservation.seatingchart.model.Seat;

import java.util.List;
import java.util.Scanner;
import java.util.Set;

import static org.junit.Assert.assertThat;


public class DriverTest {

    private DefaultSeatingChart seatingChart;
    private Driver driver;

    @Before
    public void setUp() throws Exception {
        seatingChart = new DefaultSeatingChart(3, 11);
        driver = new Driver(3, 11);
    }

    @Test
    public void testCapacity() {
        int capacity = seatingChart.getRows() * seatingChart.getColumns();
        Assert.assertEquals(capacity, 33);
    }

    @Test
    public void testGetSeatsToReserve() {
        Set<Seat> seats = driver.getInitiallyReservedSeats(new Scanner("R1C1 R1C4 R2C5"));

        assertThat(seats, containsInAnyOrder(new Seat(1, 1), new Seat(1, 4), new Seat(2, 5)));
    }

    @Test
    public void testGetReservationRequest() {
        seatingChart.reserve(new Seat(1, 1));
        seatingChart.reserve(new Seat(1, 4));
        seatingChart.reserve(new Seat(2, 5));

        List<Integer> request = driver.getReserveRequests(new Scanner("3\n1\n\n"));

        assertThat(request, contains(3, 1));
    }

}