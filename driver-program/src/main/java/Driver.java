import seatsreservation.seatingchart.DefaultSeatingChart;
import seatsreservation.seatingchart.SeatingChart;
import seatsreservation.seatingchart.model.Seat;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Integer.parseInt;

public class Driver {

    private static final int ROWS = 3;
    private static final int COLS = 11;

    private final int rows;
    private final int cols;

    private final SeatingChart seatingChart;

    public Driver(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;

        seatingChart = new DefaultSeatingChart(ROWS, COLS);
    }

    public static void main(String[] args) {
        new Driver(ROWS, COLS).run();
    }

    private void run() {
        Scanner input = new Scanner(System.in);

        Set<Seat> initiallyReservedSeats = getInitiallyReservedSeats(input);
        List<Integer> reservationRequests = getReserveRequests(input);

        reserveInitialSeats(initiallyReservedSeats);

        try {
            reservationRequests.stream()
                    .map(seatingChart::reserve)
                    .forEach(this::displayReserved);
        } catch (IllegalArgumentException e) {
            System.out.println("Not Available");
        }

        System.out.println("Available number of seats: " + seatingChart.getAvailableSeatsNumber());

        input.close();
    }

    /**
     * Return the number of requests to reserve
     * @param input {@link Scanner}
     * @return a list of numbers
     */
    List<Integer> getReserveRequests(Scanner input) {
        System.out.println("How many seats would you like to reserve? ");
        List<Integer> reservationRequests = new ArrayList<>();

        while (input.hasNextLine()) {
            String line = input.nextLine();
            if (line.isEmpty())
                break;

            reservationRequests.add(parseInt(line));
        }

        return reservationRequests;
    }

    /**
     * Return the Seats to be reserved
     * @param input {@link Scanner} in format RxCy
     * @return the Seats
     */
    Set<Seat> getInitiallyReservedSeats(Scanner input) {
        System.out.println("Enter the reserved seats:");
        Set<Seat> seats = new HashSet<>();

        for (String uniqueSeat : input.nextLine().split(" ")) {
            Pattern p = Pattern.compile("R(\\d)C(\\d)");
            Matcher m = p.matcher(uniqueSeat);
            if (m.matches()) {
                int row = parseInt(m.group(1));
                int column = parseInt(m.group(2));

                seats.add(new Seat(row, column));
            } else {
                System.err.println("Wrong input");
            }
        }

        return seats;
    }

    /**
     * Display the reserved Seats
     * @param seats reserved
     */
    private void displayReserved(Set<Seat> seats) {
        StringBuilder reserved = new StringBuilder("");

        int reservedSeats = seats.size();
        if (reservedSeats == 0) {
            reserved.append("Not Available");

        } else if (reservedSeats == 1) {
            Seat seat = seats.iterator().next();
            reserved.append("R").append(seat.getRow()).append("C").append(seat.getColumn());

        } else {
            LinkedList<Seat> list = new LinkedList<>(seats);
            Seat firstSeat = list.getFirst();
            Seat lastSeat = list.getLast();

            reserved.append("R").append(firstSeat.getRow()).append("C").append(firstSeat.getColumn()).append(" - ").append("R").append(lastSeat.getRow()).append("C").append(lastSeat.getColumn());
        }

        System.out.println(reserved);
    }

    /**
     * Reserve the initial Seats
     * @param seatsToReserve - a list of Seats to be reserved
     */
    private void reserveInitialSeats(Set<Seat> seatsToReserve) {
        seatsToReserve.forEach(seatingChart::reserve);
    }

}
