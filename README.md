# README #

### What is this repository for? ###

* Seats reservation algorithm 
* Version 0.1.0

### Set up ###
* Dependencies: Java 8, Maven
* Clone git repository locally
* Execute `mvn clean install` - this will compile, run the tests and build the artifact

### Running the application ###
* Execute the `main()` method from `Driver` class

### Who do I talk to? ###

* Repo owner: Adela Almasan