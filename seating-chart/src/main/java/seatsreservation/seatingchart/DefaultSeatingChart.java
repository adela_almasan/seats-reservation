package seatsreservation.seatingchart;

import org.apache.commons.lang3.Validate;
import seatsreservation.seatingchart.model.Seat;
import seatsreservation.seatingchart.util.SeatRowPriorityComparator;

import java.util.*;

public class DefaultSeatingChart implements SeatingChart {

    private final int rows;
    private final int columns;
    private final Set<Seat> reservedSeats = new HashSet<>();

    public DefaultSeatingChart(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
    }

    /**
     * Reserve a number of requested Seats
     *
     * @param requestedSeatNumber the number of seats to be reserved
     * @return the reserved Seats
     */
    @Override
    public Set<Seat> reserve(int requestedSeatNumber) {
        Validate.inclusiveBetween(1, 10, requestedSeatNumber, "Invalid number of seats");

        HashSet<Seat> seats = new HashSet<>();

        float center = columns % 2 == 1 ? columns / 2 + 1 : (float) (columns + 1) / 2;

        Queue<Seat> queue = getQueueWithRowPriority(center);
        queue.add(new Seat(1, (columns / 2) + 1));

        while (!queue.isEmpty() && seats.isEmpty()) {
            Seat groupCenterSeat = queue.poll();

            Set<Seat> seatGroup = getSeatGroupWithCenterSeat(groupCenterSeat, requestedSeatNumber, center);

            if (isValidSeatGroup(seatGroup)) {
                seatGroup.forEach(this::reserve);
                seats.addAll(seatGroup);

            } else {
                queue.addAll(getNextSeatsToCheck(center, groupCenterSeat));
            }
        }

        return seats;
    }

    /**
     * Reserve a valid {@link Seat}
     *
     * @param seat {@link Seat} to be reserved
     * @return true/false
     */
    @Override
    public boolean reserve(Seat seat) {
        validateSeat(seat);
        return reservedSeats.add(seat);
    }

    /**
     * Check if a {@link Seat} is already reserved
     *
     * @param seat to be verified
     * @return true/false
     */
    @Override
    public boolean isReserved(Seat seat) {
        return reservedSeats.contains(seat);
    }

    /**
     * Get the available seats after all the reservations has been made
     *
     * @return the number of available seats in the theatre
     */
    @Override
    public int getAvailableSeatsNumber() {
        return rows * columns - reservedSeats.size();
    }

    /**
     * Get the seat group with the given center seat for the group
     *
     * @param centerSeat          the group's center seat
     * @param requestedSeatNumber number of seats to be reserved
     * @param center              the center of the row
     * @return the computed group of seats
     */
    private Set<Seat> getSeatGroupWithCenterSeat(Seat centerSeat, int requestedSeatNumber, float center) {
        Set<Seat> seatGroup = new HashSet<>();

        int add = (requestedSeatNumber - 1) / 2;

        int centerSeatColumn = centerSeat.getColumn();
        for (int column = centerSeatColumn - add; column <= centerSeatColumn + add; column++) {
            seatGroup.add(new Seat(centerSeat.getRow(), column));
        }

        if (requestedSeatNumber % 2 == 0) {
            int lastSeatColumn;
            if (center == centerSeatColumn) {
                lastSeatColumn = centerSeatColumn + add + 1;
            } else {
                lastSeatColumn = centerSeatColumn < center ? centerSeatColumn + add + 1 : centerSeatColumn - add - 1;
            }

            seatGroup.add(new Seat(centerSeat.getRow(), lastSeatColumn));
        }

        return seatGroup;
    }

    /**
     * If a group is not valid, check the next seats
     *
     * @param center      the center of the row
     * @param currentSeat the current {@link Seat} from the group
     * @return the list of seats
     */
    private List<Seat> getNextSeatsToCheck(float center, Seat currentSeat) {
        List<Seat> nextSeats = new LinkedList<>();

        Set<Seat> nextHorizontalSeat = getNextHorizontalSeats(center, currentSeat);
        if (!nextHorizontalSeat.isEmpty()) {
            nextSeats.addAll(nextHorizontalSeat);
        }

        if (currentSeat.getRow() != rows) {
            nextSeats.add(new Seat(currentSeat.getRow() + 1, currentSeat.getColumn()));
        }

        return nextSeats;
    }

    /**
     * Get the next seats in row, to be checked
     *
     * @param center the center of the row
     * @param seat   the current seat
     * @return the next seats
     */
    private Set<Seat> getNextHorizontalSeats(float center, Seat seat) {
        Set<Seat> seats = new HashSet<>();

        int row = seat.getRow();
        int column = seat.getColumn();

        boolean isDeadCenter = row == 1 && column == (columns / 2) + 1;
        if (isDeadCenter) {
            if (column != 1) {
                seats.add(new Seat(row, seat.getColumn() - 1));
            }
            if (column != columns) {
                seats.add(new Seat(row, seat.getColumn() + 1));
            }

        } else if (seat.getColumn() != 1 && seat.getColumn() != columns) {
            seats.add(new Seat(row, seat.getColumn() < center ? seat.getColumn() - 1 : seat.getColumn() + 1));
        }

        return seats;
    }

    /**
     * Validate if a found group of seats is valid
     *
     * @param seatGroup teh group of seats to be validated
     * @return true/false
     */
    private boolean isValidSeatGroup(Set<Seat> seatGroup) {
        for (Seat seat : seatGroup)
            if (!isValidSeat(seat) || reservedSeats.contains(seat))
                return false;

        return true;
    }

    /**
     * Validate a {@link Seat}
     *
     * @param seat Seat to be validated
     * @throws IllegalArgumentException if the Seat is not valid
     */
    private void validateSeat(Seat seat) {
        if (!isValidSeat(seat)) {
            throw new IllegalArgumentException("Not a valid seat " + seat);
        }
    }

    /**
     * Check if a {@link Seat} is valid
     *
     * @param seat Seat to be validated
     * @return true/false
     */
    private boolean isValidSeat(Seat seat) {
        return seat.getRow() > 0 && seat.getRow() <= rows && seat.getColumn() > 0 && seat.getColumn() <= columns;
    }

    /**
     * Create a new priority queue that prioritizes front rows.
     *
     * @param center the center of the row
     * @return the queue of seats
     */
    private Queue<Seat> getQueueWithRowPriority(float center) {
        return new PriorityQueue<>(new SeatRowPriorityComparator(center));
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

}

