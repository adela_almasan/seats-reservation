package seatsreservation.seatingchart.util;

import seatsreservation.seatingchart.model.Seat;

import java.util.Comparator;

public class SeatRowPriorityComparator implements Comparator<Seat> {

    private final float center;

    public SeatRowPriorityComparator(float center) {
        this.center = center;
    }

    /**
     * Compare the distance of two seats from center
     *
     * @param o1 the first {@link Seat} to be compared
     * @param o2 the second {@link Seat} to be compared
     * @return a negative value if the first seat is on a lower row or is closer to the center
     */
    @Override
    public int compare(Seat o1, Seat o2) {
        float o1Distance = Math.abs(center - o1.getColumn()) + o1.getRow() - 1;
        float o2Distance = Math.abs(center - o2.getColumn()) + o2.getRow() - 1;

        if (o1Distance == o2Distance)
            return o1.getRow() - o2.getRow();

        return (int) (o1Distance - o2Distance);
    }

}
