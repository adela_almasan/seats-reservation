package seatsreservation.seatingchart;

import seatsreservation.seatingchart.model.Seat;

import java.util.Set;

public interface SeatingChart {

    boolean reserve(Seat seat);

    Set<Seat> reserve(int seatsNumber);

    boolean isReserved(Seat seat);

    int getAvailableSeatsNumber();

}
