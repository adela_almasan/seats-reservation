package seatsreservation.seatingchart.model;

import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class SeatTest {

    @Test
    public void testNewInstance() {
        Seat seat = new Seat(3, 5);
        assertThat(seat.getRow(), is(3));
        assertThat(seat.getColumn(), is(5));
    }

    @Test
    public void testThatTwoInstancesAreEqual() {
        Seat firstInstance = new Seat(1, 1);
        Seat differentInstance = new Seat(1, 1);
        assertThat(firstInstance, is(equalTo(differentInstance)));
    }

    @Test
    public void testThatTwoInstancesHaveTheSameHash() {
        Seat firstInstance = new Seat(1, 1);
        Seat differentInstance = new Seat(1, 1);
        assertThat(firstInstance.hashCode(), is(differentInstance.hashCode()));
    }

}
