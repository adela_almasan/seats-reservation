package seatsreservation.seatingchart;

import org.junit.Before;
import org.junit.Test;
import seatsreservation.seatingchart.model.Seat;

import java.util.Set;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class DefaultSeatingChartTest {

    private DefaultSeatingChart seatingChart;

    @Before
    public void setUp() throws Exception {
        seatingChart = new DefaultSeatingChart(4, 3);
    }

    @Test
    public void testCreatingNewInstance() {
        assertThat(seatingChart.getRows(), is(4));
        assertThat(seatingChart.getColumns(), is(3));
    }

    @Test
    public void testReserveScenario1() {
        DefaultSeatingChart chart = new DefaultSeatingChart(3, 4);

        chart.reserve(new Seat(1, 2));
        chart.reserve(new Seat(1, 3));
        chart.reserve(new Seat(2, 1));
        chart.reserve(new Seat(2, 2));
        chart.reserve(new Seat(2, 3));
        chart.reserve(new Seat(3, 2));

        Set<Seat> reservedSeats = chart.reserve(2);

        assertThat(reservedSeats, containsInAnyOrder(new Seat(3, 3), new Seat(3, 4)));
    }

    @Test
    public void testReserveExampleScenario1() {
        DefaultSeatingChart chart = new DefaultSeatingChart(3, 11);

        chart.reserve(new Seat(1, 4));
        chart.reserve(new Seat(1, 6));
        chart.reserve(new Seat(2, 3));
        chart.reserve(new Seat(2, 7));
        chart.reserve(new Seat(3, 9));
        chart.reserve(new Seat(3, 10));

        assertThat(chart.reserve(3), containsInAnyOrder(new Seat(1, 7), new Seat(1, 8), new Seat(1, 9)));
        assertThat(chart.reserve(3), containsInAnyOrder(new Seat(2, 4), new Seat(2, 5), new Seat(2, 6)));
        assertThat(chart.reserve(3), containsInAnyOrder(new Seat(3, 5), new Seat(3, 6), new Seat(3, 7)));
        assertThat(chart.reserve(1), containsInAnyOrder(new Seat(1, 5)));
    }

    @Test
    public void testTryingToReserveOverCapacity() {
        for (int row = 1; row <= seatingChart.getRows(); row++) {
            for (int column = 1; column <= seatingChart.getColumns(); column++) {
                seatingChart.reserve(new Seat(row, column));
            }
        }

        assertThat(seatingChart.reserve(1), is(empty()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTryingToReserveZeroSeats() {
        seatingChart.reserve(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTryingToReserveNegativeSeats() {
        seatingChart.reserve(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTryingToReserveMoreThanRestriction() {
        seatingChart.reserve(12);
    }

    @Test
    public void testReserveSpecificSeat() {
        Seat seat = new Seat(1, 2);

        assertThat(seatingChart.isReserved(seat), is(false));
        assertThat(seatingChart.reserve(seat), is(true));

        assertThat(seatingChart.isReserved(seat), is(true));
        assertThat(seatingChart.reserve(seat), is(false));
    }

    @Test
    public void testGetAvailableSeatsNumber() {
        DefaultSeatingChart seatingChart = new DefaultSeatingChart(3, 11);

        Set<Seat> reservedSeats = seatingChart.reserve(10);
        assertEquals(23, 33 - reservedSeats.size());
    }

}
